# Spring Boot Store

Run this project by this command : `mvn clean spring-boot:run`

Then go to your favorite browser. Then type http://localhost:8080

### Screenshot

Home Page

![Home Page](img/home.png)

Login Page

![Login Page](img/login.png)

Products List Page

![Products List Page](img/products.png)