package com.hendisantika.springbootstore.repository;

import com.hendisantika.springbootstore.entity.PurchaseOrder;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * Created by IntelliJ IDEA.
 * Project : spring-boot-store
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 13/11/18
 * Time: 07.45
 */
public interface PurchaseOrderRepository extends JpaRepository<PurchaseOrder, Integer> {
}