package com.hendisantika.springbootstore.repository;

import com.hendisantika.springbootstore.entity.Product;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

/**
 * Created by IntelliJ IDEA.
 * Project : spring-boot-store
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 13/11/18
 * Time: 07.44
 */
public interface ProductRepository extends JpaRepository<Product, Integer> {

    List<Product> findAllByTitleLikeOrDescriptionLike(String title, String description);
}