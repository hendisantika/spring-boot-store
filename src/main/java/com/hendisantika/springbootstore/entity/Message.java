package com.hendisantika.springbootstore.entity;

/**
 * Created by IntelliJ IDEA.
 * Project : spring-boot-store
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 13/11/18
 * Time: 07.47
 */
public class Message {
    private final boolean error;
    private final String message;

    public Message(String message) {
        this.error = false;
        this.message = message;
    }

    public Message(String message, boolean error) {
        this.error = error;
        this.message = message;
    }

    public boolean isError() {
        return error;
    }

    public String getMessage() {
        return message;
    }
}
